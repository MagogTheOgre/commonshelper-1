<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');

include_once ( 'php/wikiquery.php' ) ;
include_once ( 'php/peachy/Init.php' ) ;
include_once ( 'transwiki_data.php' ) ;
include_once ( 'common_data.php' ) ;
//require_once( 'php/peachy/Init.php' );

#___________________________ From common.php

# Interface localization
function load_interface_localization_sub ( $language , $lines , &$ret ) {
	global $helplink , $helptext ;
	$curlang = '' ;
	$key = '' ;
	foreach ( $lines AS $l ) {
		if ( substr ( $l , 0 , 2 ) == '==' ) { # Language section
			$curlang = strtolower ( trim ( str_replace ( '=' , '' , $l ) ) ) ;
			$key = '' ;
			continue ;
		}
		if ( substr ( $l , 0 , 2 ) == '|}' ) {
			$curlang = '' ;
			$key = '' ;
			continue ;
		}
		if ( substr ( $l , 0 , 1 ) == '!' ) { # Key?
			$key = trim ( substr ( $l , 1 ) ) ;
			if ( $language == $curlang ) $ret[$key] = '' ;
			continue ;
		}
		if ( $key == '' ) continue ;
		if ( substr ( $l , 0 , 1 ) == '|' AND substr ( $l , 0 , 2 ) != '|-' ) { # Value?
			$value = trim ( substr ( $l , 1 ) ) ;
			if ( $key == 'helptext' ) $helptext[$curlang] = $value ;
			if ( $key == 'helplink' ) $helplink[$curlang] = $value ;
			if ( $language != $curlang ) continue ;
			$ret[$key] .= $value ;
		}
	}
}

function load_interface_localization ( $url , $language ) {
	$ret = array () ;
#	do {
		$text = @file_get_contents ( $url ) ;
#	} while ( $text === false ) ;
	$lines = explode ( "\n" , $text ) ;
	$language = strtolower ( trim ( $language ) ) ;
	load_interface_localization_sub ( 'en' , $lines , $ret ) ;
	if ( $language != 'en' ) load_interface_localization_sub ( $language , $lines , $ret ) ;
	return $ret ;
}

function common_sense ( $lang , $image , $keywords = array () ) {
	$ret = real_common_sense ( $lang , $image , $keywords , array ( 'CATEGORIES' ) , true ) ;
	if ( !isset ( $ret['CATEGORIES'] ) ) return array() ;
	return $ret['CATEGORIES'] ;
}

function real_common_sense ( $lang , $image , $keywords , $sections , $force_move = false ) {
	global $forbidden_commonsense_categories ;
	$kw = array () ;
	foreach ( $keywords AS $k ) $kw[] = urlencode ( $k ) ;
	$kw = implode ( '%0D%0A' , $kw ) ;
	
	$lang2 = $lang ;
	$go = 'clean' ;
	if ( $lang != 'commons' and $force_move ) {
		$lang2 .= '.wikipedia' ;
		$go = 'move' ;
	}
	
	$url = 'http://tools.wikimedia.de/~daniel/WikiSense/CommonSense.php?u=en&' .
		'i=' . myurlencode ( $image ) .
		'&kw=' . $kw .
		'&r=on' .
		'&p=_20' .
		'&cl=' .
		'&w=' .	$lang2 .
		'&go-' . $go . '=Find+Categories' . # was : go-move
		'&v=0' ;
#	print "TESTING : " . $lang . " " . $url . "<hr/>" ;
#	do {
		$text = @file_get_contents ( $url ) ;
#	} while ( $text === false ) ;
	$bot = explode ( "\n" , utf8_decode ( $text ) ) ;
	$group = "" ;
	$cats = array () ;
	foreach ( $bot AS $l ) {
		$l = ucfirst ( trim ( $l ) ) ;
		if ( substr ( $l , 0 , 1 ) == '#' ) { # Set new group
			$group = explode ( ' ' , substr ( $l , 1 ) ) ;
			$group = array_shift ( $group ) ;
			$did_this_category = array() ;
			continue ;
		}
		if ( !in_array ( $group , $sections ) ) continue ; # Only interested in categories
		if ( trim ( $l ) == "" ) continue ; # No blank category
#		print "TESTING : $group<br/>" ;
		
		$l2 = $group . '#' . $l ;
		if ( isset ( $did_this_category[$l2] ) ) continue ; # Each category only once
		$l = str_replace ( '_' , ' ' , $l ) ;
		if ( in_array ( trim ( strtolower ( $l ) ) , $forbidden_commonsense_categories ) ) continue ;
		$did_this_category[$l2] = 1 ;
		if ( !isset ( $cats[$group] ) ) $cats[$group] = array () ;
		$cats[$group][] = $l ;
	}
	return $cats ;
}

#___________________________


function remove_image_namespace ( $image ) {
	global $image_aliases ;
	$i = strtolower ( $image ) ;
	foreach ( $image_aliases AS $ia ) {
		if ( substr ( $i , 0 , strlen ( $ia ) ) != strtolower ( $ia ) ) continue ;
		$image = array_pop ( explode ( ':' , $image , 2 ) ) ;
		break ;
	}
	return $image ;
}

function get_upload_text ( $image , $language , $project ) {
	global $is_on_toolserver , $info_templates , $license_indicators , $good_templates ;
	global $permission_texts , $use_common_sense , $is_good , $information_keys ;
	global $ignore_local_templates , $bad_headings, $license_dupes , $info_template_names ;
	global $category_names , $remove_categories , $if , $deprecated_licenses ;
	global $prevent_transfer_categories , $prevent_transfer_templates , $testing ;
	$image = remove_image_namespace ( $image ) ;
	$debug = false ;
	if ( $language == '' && $project == 'wikisource' ) $language2 = $project ;
	else if ( $project != "wikipedia" ) $language2 = "$project:$language" ;
	else $language2 = $language ;
	
	# Init
	$original_uploader = '' ;
	$first_date = '' ;
	$cat = '' ;
	$summary = '' ;

	# Run CommonSense
	$categories = '' ;
	if ( $use_common_sense ) {
    print $if['query_commonsense'] ; myflush () ;
		//print "Querying CommonSense ... " ; myflush () ;
		$categories = common_sense ( $language , $image ) ;
    print $if['done'] ; myflush () ;
		//print "done.<br/>" ; myflush () ;
	}

	# _____________________________
	# Load metadata
  print "<br/>" . $if['query_imagedata'] ; myflush () ;
	//print "Querying image data ... " ; myflush () ;
	$q = new WikiQuery ( $language , $project ) ;
	$data = $q->get_image_data ( 'File:' . $image ) ;
	$templates = $q->get_used_templates ( 'File:' . $image ) ;
	$image_cats = $q->get_categories ( 'File:' . $image ) ;
	print $if['done']."<br/>" ; myflush () ;
	//print "done.<br/>" ; myflush () ;
	
	
	# _____________________________
	# Load current text
	print $if['retrieving_image_desc'] ; myflush () ;
	//print "Retrieving image description ... " ; myflush () ;
	$iot = $is_on_toolserver ;
	$is_on_toolserver = false ;
	$text = get_wikipedia_article ( $language , 'File:' . $image , false , $project ) ;
	$text = str_replace ( '[[' , "[[:{$language2}:" , $text ) ;
	$is_on_toolserver = $iot ;
	print $if['done']."<br/>" ; myflush () ;
	//print "done.<br/>" ; myflush () ;
	
//	print $text ; myflush();
	
	# _____________________________
	# PermissionOTRS
	$otrs_data = array () ;
	$otrs_pattern = '/{{PermissionOTRS[^}]*}}/i' ;
	if ( preg_match_all ( $otrs_pattern , $text , $otrs_matches ) ) {
		foreach ( $otrs_matches AS $o ) {
			$otrs_data[] = $o[0] ;
		}
	}
	
	$otrs_orig = '' ;
	if ( $language == 'de' ) {
		$o = array () ;
		$p = '/\{\{OTRS[^}]+\}\}/' ;
		preg_match_all ( $p , $text , $o ) ;
		$q = array () ;
		foreach ( $o AS $x ) {
			$q[] = $x[0] ;
//			print $x[0] . "<br/>" ;
		}
		$otrs_orig = implode ( "\n" , $q ) ;
		$text = preg_replace ( $p , '' , $text ) ;
//		print "DEBUGGING:<pre>" ; print_r ( $text ) ; print "</pre>$otrs_orig" ;
	}
	
	# _____________________________
	# Original upload history
	
#	$data['imghistory'][] = $data['image'] ;
#	print "<hr/>TESTING, ignore!<br/><pre>" ; print_r ( $data ) ; print "</pre><hr/>" ;
	
	$users = array () ;
	$history = array () ;
	foreach ( $data['imghistory'] AS $uh ) {
		$ts = $uh['timestamp'] ;
		$comment = $uh['comment'] ;
		$user = $uh['user'] ;
		$dimensions = "{$uh['width']}&times;{$uh['height']}&times;" .  " (" . $uh['size'] . " bytes)" ; // $uh['bits'] .
		
		$users[$user] = $user ;
		$original_uploader = $user ;
		$user_link = "[[:{$language2}:User:{$user}|{$user}]]" ;
		$ts2 = explode ( ':' , $ts ) ;
		array_pop ( $ts2 ) ;
		$ts2 = implode ( ':' , $ts2 ) ;
		$ts2 = str_replace ( 'T' , ' ' , $ts2 ) ;
		$ts2 = trim ( str_replace ( 'z' , '' , $ts2 ) ) ;
		$last_date = $ts2 ;
		if ( $first_date == '' ) $first_date = $last_date ;
		
		$comment = str_replace ( "\n" , " " , $comment ) ;
		$history[$ts] = "| {$ts2} || {$dimensions} || {$user_link} || ''<nowiki>{$comment}</nowiki>''" ;
	}
	
	krsort ( $history ) ;

	$log = '{| class="wikitable"' . "\n" ;
	$log .= '! {{int:filehist-datetime}} !! {{int:filehist-dimensions}} !! {{int:filehist-user}} !! {{int:filehist-comment}}' . "\n|-\n" ;
	$log .= implode ( "\n|-\n" , $history ) ;
	$log .= "\n|}" ;
	
	
	$author = '' ;
	$m = array () ;
	if ( preg_match ( '/\|autore=(.+?)[\|\}]/' , $text , $m ) ) {
		$author = $m[1] . "\n" ;
	}
	
	
	# _____________________________
	# Author
	$orig_wiki_link = "[https://{$language}.{$project}.org {$language}.{$project}]" ;
	$desc_page = get_wikipedia_url ( $language , 'File:' . $image , '' , $project ) ;
	$author_links = "{{User at project|{$original_uploader}|{$project}|{$language}" . "}}" ;

	$author = "{{Original uploader|{$original_uploader}|{$project}|{$language}" . "}}" ;
//	$author .= "Original uploader was " . $author_links ;

	if ( count ( $users ) > 1 ) {
		$author .= ".\nLater version(s) were uploaded by " ;
		$a = array () ;
		foreach ( $users AS $u ) {
			if ( $u == $original_uploader ) continue ;
			$a[] = "[[:{$language2}:User:{$u}|{$u}]]" ;
		}
		$author .= implode ( ', ' , $a ) ;
		$author .= " at {$orig_wiki_link}." ;
	}
	if ( $language == 'nl' ) $text = str_ireplace ( '{{self|PD-auteur' , '{{self|1=PD-self' , $text ) ;
	$text = str_ireplace ( '{{self|' , '{{Self|author='.$author_links.'|' , $text ) ;
	
	
	# _____________________________
	# Source
	
	$orig_desc_link = '{{original description|' . $language . '.' . $project . '|' . urlencode ( $image ) . '}}' ;
	
	$source = "{{transferred from|{$language}.{$project}" . "}}" ;
	
	
	# _____________________________
	# Date
	if ( $first_date == $last_date ) {
		$first_date = substr ( $first_date , 0 , 10 ) ;
		$date = "{$first_date}" ;
		$date = preg_replace ( '/\b(\d{4})-(\d{2})-(\d{2})\b/' , '{{Original upload date|${1}-${2}-${3}}}' , $date ) ;
	} else {
		$first_date = substr ( $first_date , 0 , 10 ) ;
		$last_date = substr ( $last_date , 0 , 10 ) ;
		$date = "{$last_date} (first version); {$first_date} (last version)" ;
	}
	
	
	# _____________________________
	# Permission and license
	
	$is_good = false ;
	$licenses = array () ;
	$permission = array () ;
	$license_params = array() ;
	$self_tl_params = array () ;
	
	if ( false !== stripos ( $text , '{{Fair use bâtiment récent' ) ) {
		print "<div style='color:red'>Template prevents transfer to Commons!</div>" ;
		return ;
	}

	foreach ( $license_indicators AS $k => $v ) { # Checking for license indicators
		if ( false === stripos ( $text , $k ) ) continue ;
		$templates[] = $v ;
	}
	
	// The self/self2 orgy
	$p = new Template( $text, 'self' );
	if ( false === $p ) $p = new Template( $text, 'self2' );
	if ( false !== $p ) {

		foreach ( array_values($p->__get("fields")) as $v) {
			$vl = strtolower ( $v ) ;
			if ( isset ( $transwiki_templates[$language][$vl] ) ) $v = $transwiki_templates[$language][$vl] ;
			if ( !preg_match("/^\s*(migration|author)\s*\=/i", $v)) $v = ucfirst ( $v ) ;
			$self_tl_params[] = $v;
		}
		$text = $p->__get("before") . $p->__get("after");
		foreach ( $templates AS $k => $v ) {
			if ( in_array ( $v , $deprecated_licenses ) ) {
			} else if ( in_array ( $v , $self_tl_params ) or $v == 'Self' or $v == 'Self2' ) {
				unset ( $templates[$k] ) ;
				$vl = strtolower ( $v ) ;
				if ( isset ( $permission_texts[$v] ) ) {
					$is_good = true ;
					$permission[$permission_texts[$v]] = $permission_texts[$v] ;
				} else if ( in_array ( $vl , $good_templates ) or substr ( $vl , 0 , 2 ) == 'cc' OR substr ( $vl , 0 , 2 ) == 'pd' ) {
					$vu = strtoupper ( $v ) ;
					$permission[$vu] = $vu ;
					$is_good = true ;
				}
			}
		}
	}

	
	foreach ( $templates AS $t ) {
		$ot = $t ;
		$t = get_commons_template ( $language , $t ) ;
		$t = fix_self_template ( $t , $text ) ;
		if ( $debug ) print "<li>$t</li>\n" ;

		if ( in_array ( $t , $info_templates ) ) continue ; # Not a license
		if ( in_array ( $t , $info_template_names ) ) continue ; # Not a license
		$t2 = strtolower ( $t ) ;
		$tx = 'Template:' . ucfirst ( $t ) ;

	    $add_params = array () ;
    	$ot2 = str_replace ( '/' , '\/' , $ot ) ;
		$pattern = "/\{\{" . $ot2 . "\|([^\}]*)\}\}/" ;
		if ( preg_match ( $pattern , $text , $add_params ) ) {
			if ( isset ( $add_params[1] ) ) $add_params = $add_params[1] ;
			else $add_params = '' ;
		} else $add_params = '' ;
		$license_params[$tx] = $add_params ;
		
		if ( false === strpos ( $tx , '|' ) ) {
			$licenses[$tx] = $tx ;
		} else {
			$tx2 = explode ( '|' , $tx , 2 ) ;
			$tn = array_shift ( $tx2 ) ;
			$p = array_pop ( $tx2 ) ;
			$license_params[$tn] = $p . "|" . $license_params[$tx] ;
			$license_params[$tx] = $p . "|" . $license_params[$tx] ;
			$licenses[$tn] = $tn ;
		}
		
		$pattern = str_replace ( '_' , ' ' , $ot ) ;
		$pattern = str_replace ( ' ' , '[ _]' , $pattern ) ;
		$pattern = str_replace ( '/' , '\/' , $pattern ) ;
		$pattern = str_replace ( ')' , '\)' , $pattern ) ;
		$pattern = str_replace ( '(' , '\(' , $pattern ) ;
		$pattern = "/\{\{" . $pattern . "[^\}]*\}\}/i" ;
	    $text = preg_replace ( $pattern , '' , $text ) ;
		
		$tu = strtoupper ( $t ) ;
		if ( in_array ( ucfirst ( $t ) , $deprecated_licenses ) and $project == 'wikipedia' ) {
		} else if ( isset ( $permission_texts[ucfirst($t)] ) ) {
			$permission[$permission_texts[ucfirst($t)]] = $permission_texts[ucfirst($t)] ;
			$is_good = true ;
		} else if ( in_array ( $t2 , $good_templates ) ) {
			$permission[$tu] = $tu ;
			$is_good = true ;
		} else if ( substr ( $t2 , 0 , 2 ) == 'cc' OR substr ( $t2 , 0 , 2 ) == 'pd' ) {
			$permission[$tu] = $tu ;
			$is_good = true ;
		}
//		print "!!TEST:$t:$is_good!!" ;
	}
	
	// Check for preventive categories
	foreach ( $image_cats AS $c ) {
		foreach ( $prevent_transfer_categories AS $p ) {
			if ( substr ( $c , 0 , strlen ( $p ) ) == $p ) {
				$is_good = false ;
				print "<div class='alert alert-danger' role='alert'>Presence of category \"$c\" prevents transfer to Commons!</div><br/>" ;
				break ;
			}
		}
	}
	
	foreach ( $templates AS $t ) {
		foreach ( $prevent_transfer_templates AS $p ) {
			if ( substr ( $t , 0 , strlen ( $p ) ) == $p ) {
				$is_good = false ;
				print "<div class='alert alert-danger' role='alert'>Presence of template \"$t\" prevents transfer to Commons!</div><br/>" ;
				break ;
			}
		}
	}
	
	
	if ( isset ( $permission[$permission_texts['GFDL 1.2']] ) ) unset ( $permission[$permission_texts['GFDL']] ) ;
	
	// Remove templates local to the source wiki
	foreach ( $licenses AS $k => $v ) {
		foreach ( $ignore_local_templates AS $ilt ) {
			$ilt2 = 'Template:' . ucfirst ( $ilt ) ;
			if ( $v == $ilt2 ) {
				unset ( $licenses[$k] ) ; 
				break ;
			}
		}
	}
  foreach ( $ignore_local_templates AS $ilt ) {
    unset ( $permission[strtoupper($ilt)] ) ;
  }
	
	// Which of these license templates do exist on commons?
	$q = new WikiQuery ( 'commons' , 'wikimedia' ) ;
	$licenses2 = $q->get_existing_pages ( $licenses ) ;
	
	
  // Stitch license templates and their parameters back together
	foreach ( $licenses2 AS $k => $v ) {
//		if ( $testing ) print "DEBUG INFO : kv $k/$v<br/>" ;
		if ( isset ( $license_params[$v] ) && $license_params[$v] != '' ) {
			$s = $license_params[$v] ;
			while ( substr ( $s , -1 , 1 ) == '|' ) $s = substr ( $s , 0 , -1 ) ;
			$licenses2[$k] = $v . '|' . $s ;
//		if ( $testing ) print "$v : $s<br/>" ;
		}
	}
	
//	print "DEBUG INFO : licenses2 : " . implode ( ", " , $licenses2 ) . "<br/>" ;
	
	
	$license = array() ;
	$self_subs = array () ;
	foreach ( $licenses2 AS $l ) {
		$l2 = array_shift ( explode ( '|' , $l , 2 ) ) ;
		unset ( $licenses[$l2] ) ;
		$l = array_pop ( explode ( ':' , $l , 2 ) ) ;		
		$license[strtolower($l)] = '{{' . $l . '}}' ;

		# Hack to fix "self" licenses
		$ll = strtolower ( $l ) ;
		if ( substr ( $ll , 0 , 5 ) == "self|" or substr ( $ll , 0 , 6 ) == "self2|" ) {
		  $n = explode ( "|" , $l ) ;
		  array_shift ( $n ) ; # Self
		  $self_subs[] = "lang" ;
		  if ( substr ( $ll , 0 , 6 ) == "self2|" ) $self_subs[] = "self" ;
			  foreach ( $n AS $v ) {
				$self_subs[] = strtolower ( $v ) ;
				$v = array_shift ( explode ( "-" , $v ) ) ;
				$self_subs[] = strtolower ( $v ) ;
		  }
		}
	}
	
	# Fix cc-by-sa-any
	if ( isset ( $license['cc-by-sa-any'] ) ) {
	    foreach ( $license AS $k => $v ) {
	      if ( $k == 'cc-by-sa-any' ) continue ;
	      if ( substr ( $k , 0 , 8 ) == 'cc-by-sa' ) unset ( $license[$k] ) ;
	    }
	}
	
	# Fix self "sub-licenses"
	foreach ( $self_subs AS $v ) {
//		print "DEBUG : $v<br/>" ;
	    if ( isset ( $license[$v] ) )
	    	unset ( $license[$v] ) ;
	}

	# Fix lonely "Self" ;
	foreach ( $license AS $k => $v ) {
		$k2 = strtolower ( $k ) ;
//		print "DEBUG $k $v<br/>" ;
		if ( $k2 == "self" or $k2 == "self2" ) unset ( $license[$k] ) ;
	}

		
	$license = implode ( "\n" , $license ) ;
	if ( count ( $licenses ) > 0 ) {
		$license .= "\n<!-- Templates \"" . 
					implode ( '", "' , $licenses ) . 
					"\" were used in the original description page as well , but do not appear to exist on commons. -->" ;
	}
	
	# Fix *-self
	if ( $language == "en" ) $license = str_replace ( '{{GFDL-self}}' , '{{GFDL-user-en-no-disclaimers|'.$original_uploader.'}}' , $license ) ;
	
#	if ( $language == "it" ) {
#    $license = str_ireplace ( '{{PD-Utente}}' , '{{PD-user-w|it|Italian Wikipedia|'.$original_uploader.'}}' , $license ) ;
#  }


	if ( $language == 'sr' ) {
		$license = str_replace ( '{{GFDL-self}}' , '{{GFDL-user-w|sr|Wikipedia|'.$original_uploader.'}}' , $license ) ;
		$license = preg_replace ( '/\{\{PD-self(\|date\=[\w\s]+)?\}\}/' , '{{PD-user-w|'.$language.'|'.$project.'|'.$original_uploader.'}}' , $license ) ;
	} else {
		$license = str_replace ( '{{GFDL-self}}' , '{{GFDL-user-'.$language.'|'.$original_uploader.'}}' , $license ) ;
		$license = preg_replace ( '/\{\{PD-self(\|date\=[\w\s]+)?\}\}/' , '{{PD-user-w|'.$language.'|'.$project.'|'.$original_uploader.'}}' , $license ) ;
	}


  if ( false !== stripos ( $license , '{{GFDL-with-disclaimers}}' ) ) $license = trim ( str_replace ( '{{GFDL}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-en}}' ) ) $license = trim ( str_replace ( '{{GFDL}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-it}}' ) ) $license = trim ( str_replace ( '{{GFDL}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-Utente}}' ) ) $license = trim ( str_replace ( '{{GFDL-Utente}}' , "{{GFDL-user-it|$original_uploader}}" , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL 1.2}}' ) ) $license = trim ( str_replace ( '{{GFDL}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-1.2-en}}' ) ) $license = trim ( str_replace ( '{{GFDL}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-user-en-no-disclaimers}}' ) ) $license = trim ( str_replace ( '{{GFDL-self-no-disclaimers}}' , '' , $license ) ) ;
  if ( false !== stripos ( $license , '{{GFDL-user-en-with-disclaimers}}' ) ) $license = trim ( str_replace ( '{{GFDL-self-en}}' , '' , $license ) ) ;

  $license = str_replace ( '-disclaimers}}' , '-disclaimers|'.$original_uploader.'}}' , $license ) ;
  
  
  if ( count ( $self_tl_params ) > 0 ) {
  	$license .= "\n{{self|" . implode ( '|' , $self_tl_params ) . "}}" ;
  }
  
	
	# Remove pseudo-dupe licenses like GFDL and GFDL-self
	foreach ( $license_dupes AS $l1 => $l2 ) {
		if ( false === stripos ( $license , '{{' . $l1 ) ) continue ;
		$license = trim ( str_replace ( '{{' . $l2 . '}}' , '' , $license ) ) ;
		$license = trim ( str_replace ( "\n\n" , "\n" , $license ) ) ;
	}
	
	$permission = implode ( '; ' , $permission ) ;
	if ( $permission != '' ) $permission .= '.' ;
	
	
	# _____________________________
	# Description
	$description = trim ( $text ) ;
	do {
		$odesc = $description ;
		if ( substr ( $description , 0 , 1 ) == '=' ) {
			$description = trim ( array_pop ( explode ( "\n" , $description , 2 ) ) ) ;
			continue ;
		}
		$d = explode ( "\n" , $description ) ;
		$dl = array_pop ( $d ) ;
		if ( substr ( $dl , 0 , 1 ) != '=' ) array_push ( $d , $dl ) ;
		$description = trim ( implode ( "\n" , $d ) ) ;
	} while ( $description != $odesc ) ;
	if ( substr ( $description , 0 , 1 ) == '*' OR substr ( $description , 0 , 1 ) == '#' )
		$description = "\n" . $description ;

	if ( $remove_categories ) {
		$pattern = "/\[\[\:" . $language2 . "\:" . $category_names[$language] . "\:.*?]]/i" ;
		$description = preg_replace ( $pattern , '' , $description ) ;
		$pattern = "/\[\[\:" . $language2 . "\:" . $category_names['en'] . "\:.*?]]/i" ;
		$description = preg_replace ( $pattern , '' , $description ) ;
		$description = trim ( $description ) ;
	}
	
	# _____________________________
	# This is for testing only; it should not affect the live tool
	if ( substr ( $description , 0 , 12 ) == '{{NowCommons' ) {
		$a = explode ( "\n" , $description , 2 ) ;
		$description = trim ( array_pop ( $a ) ) ;
	}
	
	$description = str_replace ( ":$language::$language:" , ":$language:" , $description ) ;
	$description = str_replace ( "|\n|" , "\n|" , $description ) ;

	# _____________________________
	# Extract data from {{Information}}
	global $info_templates ;
	$other_versions = '' ;
	if ( isset ( $info_template_names[$language] ) ) $info = '{{' . $info_template_names[$language] ;
	if ( isset ( $info_template_names[$language] ) AND false !== stripos ( $description , $info ) ) {
//		print "DEBUGGING:<pre>" ; print_r ( $desription ) ; print "</pre>" ;
		$localcat = '[[:' . $language . ':category:' ;
		$d2 = $info . array_pop ( explode ( $info , $description , 2 ) ) ;
		$d2 = str_replace ( "|\n" , "\n|" , $d2 ) ;
		$d2 = str_replace ( "\n\n" , "\n" , $d2 ) ;
		$d = explode ( "\n" , $d2 ) ;
		$description = array() ;
		$mode = 'in' ;
		$key = '' ;
		$data = array() ;
		$data['Beschreibung'] = '' ;
		$data['Quelle'] = '' ;
		$data['Urheber'] = '' ;
		$data['Datum'] = '' ;
		$data['Genehmigung'] = '' ;
		$data['Andere Versionen'] = '' ;
		foreach ( $d AS $l ) {
			if ( $mode == 'out' ) {
#	print "<pre style='border:3px solid green'>TESTING, IGNORE: $l</pre>" ;
				$description[] = $l ;
				continue ;
			}
			$l = trim ( $l ) ;
			if ( $l == '}}' ) {
			$lkey = strtolower($key) ;
			if ( isset ( $information_keys[$lkey] ) ) $key = ucfirst ( $information_keys[$lkey] ) ;
				if ( $key != '' AND $cat != '' ) $data[$key] = $cat ;
				$mode = 'out' ;
				continue ;
			}
			if ( substr ( $l , 0 , 1 ) != '|' ) {
				$cat .= "\n" . $l ;
				continue ;
			}
			$lkey = strtolower($key) ;
			if ( isset ( $information_keys[$lkey] ) ) $key = ucfirst ( $information_keys[$lkey] ) ;
			if ( $key != '' AND $cat != '' ) $data[$key] = $cat ;
			$l = explode ( '=' , $l , 2 ) ;
			$key = trim ( substr ( array_shift ( $l ) , 1 ) ) ;
			$cat = trim ( array_pop ( $l ) ) ;
		}
		if ( $cat != '' ) $data[$key] = $cat ;
		$description = implode ( "\n" , $description ) ;
#	print "<pre style='border:3px solid yellow'>TESTING, IGNORE: $key / $cat / $description</pre>" ;
		fix_localized_info_table ( $language , $data ) ;
		$description = add_original_description ( $description , $data['Beschreibung'] , 1 ) ;
		$source = add_original_description ( $source , $data['Quelle'] ) ;
		$date = add_original_description2 ( $date , $data['Datum'] ) ;
		$author = trim ( add_original_description ( '' , $data['Urheber'] ) . ".\n$author" ) ;
		$permission = add_original_description ( $permission , $data['Genehmigung'] ) ;
		$other_versions = add_original_description ( $other_versions , $data['Andere Versionen'] ) ;
	} else if ( $language == 'de' ) { # Attempt to freestyle-parse German text...
    $beginnings = array ( '*' , "'" , ' ' ) ;
    $conts = array ( "'" , ':' , ' ' ) ;
    $letters = array () ;
    for ( $a = 'a' ; $a <= 'z' ; $a++ ) $letters[] = $a ;
    for ( $a = 'A' ; $a <= 'Z' ; $a++ ) $letters[] = $a ;
    $letters[] = '/' ;
    $lines = explode ( "\n" , $description ) ;
    $description = '' ;
    foreach ( $lines AS $line ) {
      $orig_line = $line ;
      $key = '' ;
      while ( $line != '' AND in_array ( $line[0] , $beginnings ) ) $line = substr ( $line , 1 ) ;
      while ( $line != '' AND in_array ( $line[0] , $letters ) ) {
        $key .= $line[0] ;
        $line = substr ( $line , 1 ) ;
      }
      while ( $line != '' AND in_array ( $line[0] , $conts ) ) $line = substr ( $line , 1 ) ;
      $key = strtoupper ( $key ) ;
      $line = ucfirst ( trim ( $line ) ) ;
      if ( $key == 'QUELLE' ) $source = add_original_description ( $source , $line ) ;
      else if ( $key == 'LIZENZSTATUS' ) $permission = add_original_description ( $permission , $line ) ;
      else if ( $key == 'LIZENZ' ) $permission = add_original_description ( $permission , $line ) ;
      else if ( $key == 'DATUM' ) $date = add_br ( $line , $date ) ;
      else if ( $key == 'FOTOGRAF' ) $author = add_br ( $line , $author ) ;
      else if ( $key == 'ZEICHNER' ) $author = add_br ( $line , $author ) ;
      else if ( $key == 'FOTOGRAF/ZEICHNER' ) $add_br = trim ( $line , $author ) ;
      else if ( $key == 'BESCHREIBUNG' ) $description = add_original_description ( $description , $line ) . "\n" ;
      else if ( $key == 'BILDBESCHREIBUNG' ) $description = add_original_description ( $description , $line ) . "\n" ;
      else $description .= $orig_line . "\n" ;
    }
    $description = trim ( $description ) ;
	}
	

	# Remove certain headings from description
	$desc = explode ( "\n" , $description ) ;
	$description = '' ;
	foreach ( $desc AS $d ) {
		$d2 = trim ( strtolower ( str_replace ( '=' , '' , $d ) ) ) ;
		if ( in_array ( $d2 , $bad_headings ) ) continue ; # Removing it from the description
		$description .= $d . "\n" ;
	}
	$description = trim ( $description ) ;


	# _____________________________
	# Final fixes
	if ( $description == '' ) $description = "''no original description''" ;
	$permission = trim ( $permission . "\n" . implode ( "\n" , $otrs_data ) ) ;
	if ( $permission == '' ) $permission = "''See license section.''" ;
	$license = trim ( str_replace ( '{{Author}}' , '' , $license ) ) ;
	
	$permission = trim ( $permission . "\n" . $otrs_orig ) ;


	$orig_cats = '' ;
	if ( !$remove_categories && isset ( $category_names[$language] ) ) {
		$a1 = array () ;
		$pattern = "/\[\[\:" . $language2 . "\:(" . $category_names[$language] . "\:.*?)]]/i" ;
		preg_match_all ( $pattern , $description , $a1 ) ;
		$a2 = array () ;
		$pattern = "/\[\[\:" . $language2 . "\:(" . $category_names['en'] . "\:.*?)]]/i" ;
		preg_match_all ( $pattern , $description , $a2 ) ;
		
		if ( isset ( $a1[1] ) ) {
			foreach ( $a1[1] AS $c ) $orig_cats .= "[[" . $c . "]]\n" ;
		}
		if ( isset ( $a2[1] ) ) {
			foreach ( $a2[1] AS $c ) $orig_cats .= "[[" . $c . "]]\n" ;
		}
		
	}

  if ( false !== stripos ( $description , '{{Nach Commons verschieben (bestätigt)' ) ) {
  	$description = explode ( '{{Nach Commons verschieben (bestätigt)' , $description ) ;
  	$description[1] = array_pop ( explode ( '}}' , $description[1] , 2 ) ) ;
  	$description = trim ( implode ( '' , $description ) ) ;
  }
  

	
	$fin = "__NOTOC__\n" ;
#	if ( $project == "wikipedia" ) $fin .= "{{Media from Wikipedia|lang=$language}}\n" ;
	if ( is_array ( $categories ) AND count ( $categories ) > 0 ) {
		$fin .= "\n[[Category:" . utf8_encode ( implode ( "]]\n[[Category:" , $categories ) ) . ']]' ;
	} else if ( $orig_cats != '' ) {
		$fin .= "\n" . $orig_cats ;
  } else {
    $fin .= "\n{{subst:Unc}} <!-- Remove this line once you have added categories -->" ;
  }
  if ( $language != '' && array_shift(explode('|',$description,2)) != '{{'.$language ) $description = '{{' . $language . '|' . $description . '}}' ;
  
	$license = preg_replace ( $otrs_pattern , '' , $license ) ;
	$license = str_replace ( '$$UPLOADER$$' , $author_links , $license ) ;
	
//	$date = preg_replace ( '/\b(\d{4})-(\d{2})-(\d{2})\b/' , '{{Original upload date|${1}-${2}-${3}}}' , $date ) ;


	
	# _____________________________
	# Output
	$out = "
	== {{int:filedesc}} ==
	{{Information
	|Description={$description}
	|Source={$source}
	|Date={$date}
	|Author={$author}
	|Permission={$permission}
	|other_versions={$other_versions}
	}}
	
	== {{int:license-header}} ==
	{$license}
	
	== {{Original upload log}} ==
	{$orig_desc_link}
	{$log}
	{$fin}" ; //  All following user names refer to {$language}.{$project}.
	

	// Remove unwanted categories	
	$remcats = array ( 'Hidden categories' , 'Stub icons' ) ;
	foreach ( $remcats AS $r ) {
		$out = str_ireplace ( '[[Category:'.$r.']]' , '' , $out ) ;
		$r = str_replace ( ' '  , '_' , $r ) ;
		$out = str_ireplace ( '[[Category:'.$r.']]' , '' , $out ) ;
	}
//	$out = str_ireplace ( '[[Category:Hidden categories]]' , '' , $out ) ;
//	$out = str_ireplace ( '[[Category:Hidden_categories]]' , '' , $out ) ;

	
	$out = str_ireplace ( '{{PD}}' , '' , $out ) ;
	$out = str_replace ( '|Migration=relicense' , '|migration=relicense' , $out ) ;
	
	$pattern = '/\[\[(:'.$language2.':[^|\]]+)\]\]/' ;
	$out = preg_replace ( $pattern , '[[${1}|]]' , $out ) ;

	$out = str_ireplace ( '[[:wikiversity:beta:' , '[[:betawikiversity:' , $out ) ;

	if ( $otrs_orig != '' ) $out = str_replace ( '{{OTRS}}' , '' , $out ) ;
	
	$out = str_replace ( 'Http://' , 'http://' , $out ) ; // Oddity

	return str_replace ( "\t" , '' , $out ) ;
}

function add_br ( $k1 , $k2 ) {
  $k1 = trim ( $k1 ) ;
  $k2 = trim ( $k2 ) ;
  if ( $k2 == '' ) return $k1 ;
  if ( $k1 == '' ) return $k2 ;
  return "$k1<br/>\n$k2" ;
}

$info_templates = array (
	'de' => 'Information',
	'fi' => 'Tiedoston tiedot',
) ;

$info_keys = array() ;
$info_keys['fi'] = array (
	'Kuvaus' => 'Bechreibung',
	'Lähde' => 'Quelle',
	'Päiväys' => 'Datum',
	'Tekijänoikeuksien haltija' => 'Urheber',
) ;

function fix_localized_info_table ( $language , &$data ) {
	global $info_keys ;
	if ( !isset ( $info_keys[$language] ) ) return ;
	foreach ( $info_keys[$language] AS $k => $v ) {
		$k2 = strtolower ( substr ( $k , 0 , 1 ) ) . substr ( $k , 1 ) ;
		$k1 = ucfirst ( $k ) ;
		$v = ucfirst ( $v ) ;
		if ( $data[$v] != '' ) continue ; # Already have that
		if ( isset ( $data[$k1] ) ) $data[$v] = $data[$k1] ;
		if ( isset ( $data[$k2] ) ) $data[$v] = $data[$k2] ;
	}
}

function add_original_description ( $desc , $orig , $translate = false ) {
  global $language ;
	$orig = trim ( $orig ) ;
	if ( $orig == '' ) return $desc ;
	$desc = trim ( $desc ) ;
	if ( $desc == '' ) return $orig ;
	if ( $translate ) {
    return "{{" . $language. "|{$orig}<br/>\n$desc}}" ;
  }
	return $desc . "<br/>\n(Original text : ''{$orig}'')" ;
}

function add_original_description2 ( $desc , $orig , $translate = false ) {
  global $language ;
	$orig = trim ( $orig ) ;
	if ( $orig == '' ) return $desc ;
	$desc = trim ( $desc ) ;
	if ( $desc == '' ) return $orig ;
	if ( $translate ) {
    return "{{" . $language. "|{$orig}<br/>\n$desc}}" ;
  }
	return "{$orig}<br/>\n($desc)" ;
}

function get_commons_template ( $language , $t ) {
	global $transwiki_templates ;
	
	$t = strtolower ( substr ( $t , 0 , 1 ) ) . substr ( $t , 1 ) ;
	if ( isset ( $transwiki_templates[$language] ) AND isset ( $transwiki_templates[$language][$t] ) ) {
		return $transwiki_templates[$language][$t] ;
	}
	$t = ucfirst ( $t ) ;
	if ( isset ( $transwiki_templates[$language] ) AND isset ( $transwiki_templates[$language][$t] ) ) {
		return $transwiki_templates[$language][$t] ;
	}
	return $t ;
}

function fix_self_template ( $template , $text ) {
	global $self_texts ;
	$tl = strtolower ( $template ) ;
	if ( $tl != 'pd' AND $tl != 'gfdl' ) return $template ;
	$text = strtolower ( str_replace ( ' ' , '' , $text ) ) ;
	foreach ( $self_texts AS $st ) {
		if ( false === strpos ( $text , $st ) ) continue ;
		$template .= '-self' ;
		return $template ;
	}
	return $template ;
}


function show_form () {
	global $language , $project , $image , $if , $newname, $use_common_sense, $remove_categories;
	global $du , $rdu , $ignorewarnings , $testing ;
	$ucs = $use_common_sense ? ' checked ' : '' ;
	$urc = $remove_categories ? ' checked ' : '' ;
	$checked_directupload = ( $du or $rdu ) ? "checked" : "" ;
	$checked_ignorewarnings = ( $ignorewarnings ) ? "checked" : "" ;
	
	$form = "<form method='post' action='?' class='form' >" ;
	$form .= "<table class='table table-condensed'><tbody>" ;
	$form .= "<tr><td nowrap>" . $if['langcode'] . "</td><td colspan=2>" ;
	$form .= "<div class='col-sm-2'><input type=text name=language value='{$language}' /></div><div class='col-sm-1'>.</div><div class='col-sm-2'><input type=text name=project value='{$project}' /></div> . org</td></tr>" ;
//	$form .= "<tr><td>" . $if['project'] . "</td><td><input type=text name=project value='{$project}' cols=30/></td><td>" . $if['projectnote'] . "</td></tr>" ;
	$form .= "<tr><td>" . $if['imgname'] . "</td><td colspan=2><input type=text name=image value='{$image}' class='span5' /></td></tr>" ;
	$form .= "<tr><td>" . $if['newname'] . "</td><td colspan=2><input type=text name=newname value='{$newname}' class='span5'/> " . $if['directnote']  . "</td></tr>" ;
	$form .= "<tr><td/><td colspan=2>" . "<label class='checkbox inline' for='remove_categories'><input type=checkbox name=remove_categories id='remove_categories' value=1 {$urc} /> " . $if['removecategories'] . "</label><br/>" ;
	$form .= "<tr><td/><td colspan=2>" . "<label class='checkbox inline' for='ignorewarnings'><input type=checkbox name=ignorewarnings value=1 id='ignorewarnings' $checked_ignorewarnings/> Ignore warnings (overwrite existing images)</label><br/>" ;
	$form .= "</tbody></table>" ;
	
	$form .= "<div style='border:2px solid gray;padding:2px;margin:2px'>" ;
	$form .= "<label for='du'><input checked type=checkbox name=reallydirectupload value=1 id='du' $checked_directupload/> " . $if['reallydirectupload'] . "</label><br/>" ;
	$form .= "To use the automatic transfer function, you need to authorize <a href='/magnustools/oauth_uploader.php?action=authorize' target='_blank'>OAuth uploader</a> to perform uploads under your Commons user name.<br/>See also <a href='http://blog.magnusmanske.de/?p=183'>this blog entry</a>." ;
	$form .= "</div><br/>" ;
	
	$form .= "<input class='btn btn-primary' type=submit name=doit value='" . $if['gettext'] . "'/>" ;
	$form .= "<input type=hidden name=test value=$testing />" ;
	$form .= "</form>" ;
	
	print $form ;
}

function get_upload_form ( $image , $language , $project , $text ) {
	global $is_good , $if , $directupload ;
	$action = 'https://commons.wikimedia.org/wiki/Special:Upload' ;
	$uform = "<form id='upload' method=post enctype='multipart/form-data' action='{$action}'>" ;
	$uform .= "<textarea style='width:100%' rows='20' name='wpUploadDescription'>" ;
	$uform .= $text ;
	$uform .= "</textarea>" ;
	
	
	$image_url = "https:" . get_image_url ( $language , utf8_decode ( $image ) , $project ) ;
	if ( $language == '' and $project == 'wikisource' ) $is_good = true ;
	if ( $is_good ) {
    $s1 = $if['good_license'] ;
    $s1 = str_replace ( "$1" , "<a href='$image_url'>$image</a>" , $s1 ) ;
    $s1 = str_replace ( "$2" , "<input type='submit' name='fake_upload' value='" , $s1 ) ;
    $s1 = str_replace ( "$3" , "' />" , $s1 ) ;
    $uform .= $s1 ;
//		$uform .= "Save <a href='" . $image_url ."'>{$image}</a> locally, then " ;
//		$uform .= "<input type='submit' name='fake_upload' value='upload at commons' />" ;
	} else {
		$uform .= "<font color='red'>" . $if['bad_license'] . "</font>" ;
	}
	if ( $directupload ) {
		$uform .= "<input type='hidden' name='lang' value='{$lang}'/>" ;
		$uform .= "<input type='hidden' name='image' value='" . urlencode ( $image ) . "'/>" ;
		$uform .= "<input type='hidden' name='reallydirectupload' value='1'/>" ;
		$uform .= "<input type='hidden' name='directupload' value='1'/>" ;
		$uform .= "<input type='hidden' name='doit' value='1'/>" ;
	}
	$uform .= "</form>" ;
	return $uform ;
}

function show_nowcommons_button () {
	global $language , $project , $image , $newname ;
	$button_label = 'Add {{NowCommons}}' ;
	$title = "File:$image" ;
	$tn = "NowCommons" ;
	$ip = "" ;
	if ( $language == 'en' ) {
		$tn = 'subst:ncd' ;
		$ip = "File:" ;
	} else $tn = "NowCommons" ;
	
	if ( $image == $newname ) $text = '{{' . $tn . '}}' ;
	else $text = '{{' . $tn . '|' . $ip . $newname . '}}' ;
	
	$stuff = urlencode("File:$image")."&action=delete&wpReason=NowCommons%20[[:commons:File:".urlencode($newname)."]]";
	$summary = 'Now commons' ;
	
	print "<br/>" . cGetEditButton ( $text , $title , $language , $project , $summary , $button_label , false , true , false , false ) ;
	print " or <a href=\"https://$language.$project.org/w/index.php?title=$stuff\">delete the original image</a> straight away." ;
	
	print " <a id='newfile_link' href=''>Your new file should be here!</a>" ;
}

# ____________________________________________________________________________________________________________________
# Main program

# _____________________________
# Initialize
$image = trim ( remove_image_namespace ( trim ( get_request ( 'image' ) ) ) ) ;
$language = trim ( get_request ( 'language' ) ) ;
if ( $language == '' ) $language = get_request ( 'lang' , 'en' ) ;
$project = trim ( get_request ( 'project' , 'wikipedia' ) ) ;

/*
$language = fix_language_code ( $language ) ;
$project = check_project_name ( $project ) ;
*/

$interface_language = get_request ( 'interface' , 'en' ) ;
$ignorewarnings = get_request ( 'ignorewarnings' , false ) ;
$use_common_sense = isset ( $_REQUEST['commonsense'] ) ;
$remove_categories = isset ( $_REQUEST['remove_categories'] ) ;
$newname = get_request ( 'newname' , $image ) ;
$orig_newname = $newname ;
if ( $newname == "" ) $newname = $image ;
$du = get_request ( 'directupload' , false ) ;
$rdu = get_request ( 'reallydirectupload' , false ) ;
$doit = isset ( $_REQUEST['doit'] ) ;
$testing = isset ( $_REQUEST['test'] ) ;
if ( !$doit ) $use_common_sense = true ;
if ( $du ) $rdu = true ;
if ( $use_common_sense AND isset ( $_REQUEST['commonsense'] ) AND $_REQUEST['commonsense'] == '0' ) $use_common_sense = false ;
if ( $remove_categories AND $_REQUEST['remove_categories'] == '0' ) $remove_categories = false ;

$newname = explode ( '.' , $newname ) ;
if ( count ( $newname ) > 1 ) {
  $extension = trim ( array_pop ( $newname ) ) ;
  $last = rtrim ( array_pop ( $newname ) ) ;
  $newname[] = $last ;
  $newname[] = $extension ;
}
$newname = implode ( '.' , $newname ) ;
# i18n

$helptext = array () ;
$helplink = array () ;
$if = load_interface_localization ( 'https://commons.wikimedia.org/w/index.php?title=User:Magnus_Manske/Commonshelper_interface&action=raw' , 
		$interface_language ) ;

$help = array() ;
foreach ( $helptext AS $hl => $ht ) {
	$link = $helplink[$hl] ;
	$help[] = "<a target='_blank' href='{$link}'>{$ht}</a>" ;
}
$help = implode ( ' | ' , $help ) . "<br/>" ;

$interface_links = array () ;
foreach ( array_keys($helptext) AS $l ) {
	$url = "?interface={$l}" ;
	$interface_links[] = "<a href='$url'>" . strtoupper ( $l ) . "</a>" ;
}
$interface_links = implode ( ' | ' , $interface_links ) ;


# _____________________________
# Problems?
$tophint = '' ;


if ( $doit ) {
	$wq2 = new WikiQuery ( $language , $project ) ;
	if ( !$wq2->does_image_exist ( "File:$image" ) ) {
    $tophint = "Image \"$image\" does not exist on $language.$project!" ;
    $doit = false ;
	}
}


$newname2 = $newname ;
/*$newname = ansi2ascii ( $newname ) ;
if ( $doit and $newname != $newname2 ) {
	$tophint = "Image name has been ASCIIfied from \"$newname2\" to \"$newname\"!" ;
	if ( !$ignorewarnings ) $doit = false ;
}*/

if ( $doit ) {
  if ( $language == "xxx" ) $language = "" ;
	$wq = new WikiQuery ( "commons" , "wikimedia" ) ;
	$newname2 = $newname ;
	$count = 1 ;
#	print "!!$newname2!!" ;
	while ( $wq->does_image_exist ( "File:$newname2" ) ) {
		$n = explode ( '.' , $newname ) ;
		$newname2 = " ($count)." . array_pop ( $n ) ;
		$newname2 = implode ( '.' , $n ) . $newname2 ;
		$count++ ;
	}
	if ( $newname2 != $newname ) {
		$url1 = "https://commons.wikimedia.org/wiki/File:" . myurlencode ( $newname ) ;
		$url2 = get_thumbnail_url ( "commons" , $newname , 120 , "wikimedia" ) ;
		$url_orig1 = "https://$language.$project.org/wiki/File:" . myurlencode ( $image ) ;
		$url_orig2 = get_thumbnail_url ( $language , $image , 120 , $project ) ;
		$dc = db_get_image_data ( $image , 'commons' , 'wikimedia' ) ;
		$do = db_get_image_data ( $image , $language , $project ) ;
		$dc = $dc->img_width . " &times; " . $dc->img_height . " px" ;
		$do = $do->img_width . " &times; " . $do->img_height . " px" ;
		$tophint = "<div style='float:right;text-align:center;border:1px solid grey'>
		<a href=\"$url1\"><img src=\"$url2\" border=0 /></a><br/><small><i>$newname</i><br/>exists on commons<br/>$dc</small>
		<hr/>
		<a href=\"$url_orig1\"><img src=\"$url_orig2\" border=0 /></a><br/><small>You are transfering<br/><i>$newname</i><br/>from $language.$project<br/>$do</small>
		</div>" ;
		
		
		
		$tophint .= "Image \"$newname\" already exists. Image name has been changed to \"$newname2\"!" ;
		$newname = $newname2 ;
		if ( !$ignorewarnings ) $doit = false ;
	}
}


$db = '' ;


function db_get_image_data ( $image , $language , $project ) {
	global $db ;
	
	$db = openDB ( $language , $project ) ;
	make_db_safe ( $image ) ;

	$sql = "SELECT * FROM image WHERE img_name=\"{$image}\"" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	if($o = $result->fetch_object()){
		return $o ;
	}
/*
	$mysql_con = db_get_con_new($language) ;
	$db = $language . 'wiki_p' ;
	
	$sql = "SELECT * FROM image WHERE img_name=\"{$image}\"" ;
	$res = my_mysql_db_query ( $db , $sql , $mysql_con ) ;
	if ( mysql_errno() != 0 ) return false ; // Something's broken
	
	if ( $o = mysql_fetch_object ( $res ) ) {
    return $o ;
  }
*/	
	return false ;
}


# _____________________________
# Header
print "<!DOCTYPE html>\n<html " ;
print "lang='$interface_language' " ;
if ( $interface_language == "he" ) print "dir='rtl' " ;
print "><body>" ;
print '<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>' ;
print get_common_header ( "commonshelper.php" , "CommonsHelper" ) ;
print "<h1>" . $if['title'] . "</h1>" ;

if ( isset ( $newname ) and isset ( $image ) and isset ( $project ) and isset ( $language ) ) {
	$ch2 = "/commonshelper2/?language=$language&project=$project&target_file=" . urlencode ( $newname ) . "&file=" . urlencode ( $image ) ;
} else $ch2 = '' ;
if ( $use_common_sense ) $ch2 .= "&use_checkusage=1" ;

print "<small><i>{$interface_links}</i><br/>" ;
print $help . $if['desc1'] . "</small><br/>" ;
print "<small><i>" . $if['loggedinwarning'] . "</i></small><br/>" ;
if ( $tophint != '' ) print "<div class='alert alert-danger' role='alert'>$tophint</div><br/>" ;
myflush () ;

if ( $doit ) {
	$upload_text = get_upload_text ( $image , $language , $project ) ;
	
	if ( $rdu ) {
		if ( !$is_good ) print "<div class='alert alert-danger' role='alert'>" . $if['bad_license'] . "</div>" ;
		else {

			$external_url = "https:" . get_image_url ( $language , $image , $project ) ;

			print '<script>
var language="'.$language.'";
var project="'.$project.'";

function urldecode(url) {
  return decodeURIComponent(url.replace(/\+/g, " "));
}

var params = {
	action:"upload",
	newfile:urldecode("'.urlencode($newname).'"),
	url:urldecode("'.urlencode($external_url).'"),
	desc:urldecode("'.urlencode($upload_text).'"),
	comment:"Transferred from "+language+"."+project,
	botmode:1
} ;

var months = {
0 : "January" ,
1 : "February" ,
2 : "March" ,
3 : "April" ,
4 : "May" ,
5 : "June" ,
6 : "July" ,
7 : "August" ,
8 : "September" ,
9 : "October" ,
10 : "November" ,
11 : "December"
} ;

$(document).ready ( function () {

	
	function showError ( msg ) {
		$("#uploading").html ( "<big>ERROR: "+msg+"</big>" ) ;
	}
	
	function doUpload () {
		$.post ( "/magnustools/oauth_uploader.php?rand="+Math.random() , params , function ( d ) {
			if ( d.error != "OK" ) {
				console.log ( params ) ;
				console.log ( d ) ;
				var msg = [] ;
				if ( d.error != null ) msg = [ d.error ] ;
				else if ( typeof d.res.error.info != "undefined" ) msg = [ d.res.error.info ] ;
				if ( typeof d.res != "undefined" ) {
					if ( typeof d.res.upload != "undefined" ) {
						if ( typeof d.res.upload.warnings != "undefined" ) {
							$.each ( d.res.upload.warnings , function ( k2 , v2 ) {
								msg.push ( "<b>" + k2 + "</b> : " + v2 ) ;
							} ) ;
						}
					}
				}
				showError ( msg.join("<br/>") ) ;
				return ;
			}
			$("#uploading").html ( "<big>Transfer successful!</big>" ) ;
			$("#newfile_link").attr("href","//commons.wikimedia.org/wiki/File:"+params.newfile) ;
			$("#now_commons_buttons").show() ;
		} , "json" ) .fail(function() { showError ( "OAuth uploader failed or does not respond" ) } ) ;
	}
	
	$.get ( "/magnustools/oauth_uploader.php?action=checkauth&botmode=1" , function ( d ) {
		if ( d.error != "OK" ) {
			alert ( "Auth not OK" ) ;
			return ;
		}
		var is_autopatrolled = false ;
		$.each ( d.data.query.userinfo.groups , function ( k , v ) {
			if ( v == "autopatrolled" ) is_autopatrolled = true ;
		} ) ;
		var now = new Date() ;
		var bmtc = "{{BotMoveToCommons|"+language+"."+project+"|"+now.getFullYear()+"|"+months[now.getMonth()]+"|"+now.getDate()+"}}" ;
		if ( !is_autopatrolled ) {
			params.desc = bmtc + "\n\n" + params.desc ;
		}
		doUpload () ;
	} , "json" ) ;

} ) ;
</script>
<div id="uploading"><i>Transfer in progress...</i></div>
' ;
//			print do_direct_upload ( $language , $image , $upload_text ) ;
			print "<div id='now_commons_buttons' style='display:none'>" ;
			show_nowcommons_button () ;
			print "</div>" ;
		}
	} else {
		print get_upload_form ( $image , $language , $project , $upload_text ) ;
	}

} else {
	show_form () ;
}

print "</body></html>" ;

?>
