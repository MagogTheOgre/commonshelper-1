<?PHP

$transwiki_templates = array () ;
$transwiki_templates['ar'] = array (
	'رخصة جنو للوثائق الحرة 1.2' => 'GFDL-1.2' ,
) ;
$transwiki_templates['bn'] = array (
	'pd Art' => 'PD-Art' ,
	'pd art' => 'PD-Art' ,
	'pd self' => 'PD-self' ,
	'pd Self' => 'PD-self' ,
	'PD-user' => 'PD-user-w|bn|wikipedia' ,
	'GFDL-user' => 'GFDL-user-w|bn|wikipedia' ,
	'GFDL-self-with-disclaimers' => 'GFDL-user-bn-with-disclaimers' ,
	'GFDL-1.2' => 'GFDL-1.2',
	'cc-by-4.0' => 'cc-by-4.0',
	'cc-by-sa-4.0' => 'cc-by-sa-4.0',
) ;
$transwiki_templates['ca'] = array (
	'DP-usuari' => 'PD-user-w|ca|wikipedia' ,
	'DP-Propi' => 'PD-self' ,
) ;
$transwiki_templates['de'] = array (
	'bild-GFDL' => 'GFDL',
	'bild-GPL' => 'GPL',
	'bild-CC-by-sa/2.0/de' => 'Cc-by-sa-2.0-de',
	'bild-CC-by/2.0/de' => 'Cc-by-2.0-de',
	'bild-PD' => 'PD',
	'bild-GFDL-OpenGeoDB' => 'GFDL-OpenGeoDB',
	'bild-GFDL-GMT' => 'GFDL', // Doesn't seem to exist on commons
	'bild-CC-by-sa/2.0' => 'Cc-by-sa-2.0',
	'bild-CC-by-sa/2.5' => 'Cc-by-sa-2.5',
	'bild-CC-by/2.0' => 'Cc-by-2.0',
	'bild-CC-by/2.5' => 'Cc-by-2.5',
	'bild-CC-by-sa/3.0' => 'cc-by-sa-3.0',
	'bild-CC-by-sa/3.0/de' => 'cc-by-sa-3.0-de',
	'bild-CC-sa/1.0' => 'Cc-by-sa',
	'bild-PD-alt' => 'PD-old',
	'bild-PD-Kunst' => 'PD-Art',
	'bild-PD-US' => 'PD-USGov',
	'bild-UN' => 'PD-UN',
	'bild-frei' => 'PD-release',
	'bild-PD-alt-100' => 'PD-old',
	'bild-PD-Schöpfungshöhe' => 'PD-ineligible',
	'bild-PD-frei' => 'PD-self',
	'bild-PD-nur PD' => 'PD-release',
	'bild-PD-alt-Vervielfältigung' => 'PD-old',
	'bild-GFDL/1.2' => 'GFDL-1.2',
	'bild-PD-Amtliches Werk' => 'PD-GermanGov',
	'bild-PD-Amtliches Werk (Deutsche Briefmarke)' => 'PD-GermanGov',
	'bild-by' => 'Attribution',
	'bild-CC-by/3.0' => 'cc-by-3.0',
	'bild-CC-by/3.0/de' => 'cc-by-3.0-de',
	'bild-CC-by-sa/1.0' => 'cc-by-sa-1.0',
	'bild-CC-by-sa/3.0/at' => 'cc-by-sa-3.0-at',
	'bild-CC-by-sa/3.0/ch' => 'cc-by-sa-3.0-ch',
	'bild-CC-by/4.0' => 'cc-by-4.0',
	'bild-CC-by-sa/4.0' => 'cc-by-sa-4.0',
	'bild-CC-0' => 'cc-zero',
	'bild-GFDL/1.3' => 'GFDL-1.3',
) ;
$transwiki_templates['el'] = array (
	'ΚΚ-70' => 'PD-Art' ,
	'ΚΚ-ΗΠΑ' => 'PD-USGov' ,
	'ΚΚ-ΗΠΑ-NASA' => 'PD-USGov-NASA' ,
	'ΚΚ' => 'PD' ,
	'ΚΚ-χρήστη' => 'PD-user-w|el|wikipedia' ,
	'GFDL-self' => 'GFDL-user-w|el|wikipedia' ,
	'ΠΔΕΧ-2' => 'attribution',
	'ΠΔΕΧ' => 'PD-author',
) ;
$transwiki_templates['en'] = array (
	'pd Art' => 'PD-Art' ,
	'pd art' => 'PD-Art' ,
	'pd self' => 'PD-self' ,
	'pd Self' => 'PD-self' ,
	'PD-user' => 'PD-user-w|en|wikipedia' ,
	'GFDL-user' => 'GFDL-user-w|en|wikipedia' ,
	'GFDL-self-with-disclaimers' => 'GFDL-user-en-with-disclaimers' ,
	'GFDL-1.2' => 'GFDL-1.2',
	'cc-by-4.0' => 'cc-by-4.0',
	'cc-by-sa-4.0' => 'cc-by-sa-4.0',
) ;
$transwiki_templates['eo'] = array (
	'Atribuo' => 'Attribution',
) ;
$transwiki_templates['es'] = array (
	'propio|GFDL' => 'GFDL-self',
	'propio|CC-by-sa-2.5' => 'CC-by-sa',
	'propio|CC-by-sa' => 'CC-by-sa',
	'propio|CC-by' => 'CC-by',
	'propio|PD' => 'PD',
	'propio|DP' => 'PD',
	'DP' => 'PD',
) ;
$transwiki_templates['fa'] = array (
	'مالکیت عمومی-خود' => 'PD-self',
	'نگاره قدیمی' => 'PD-old',
	'مجوز گنو' => 'GFDL' ,
	'مالکیت عمومی' => 'PD' ,
) ;
$transwiki_templates['fi'] = array (
	'Vapaa' => 'Copyrighted free use',
	'Vapaa/NimiMainittava' => 'Attribution',
) ;
$transwiki_templates['fr'] = array (
	'Domaine public' => 'PD',
	'Domaine public Iran' => 'PD',
	'Domaine public NARA' => 'PD-USGov-NARA',
	'Domaine public NASA' => 'PD-USGov-NASA',
	'Domaine public NSA' => 'PD-USGov-NSA',
	'Domaine public UK' => 'PD-BritishGov',
	'Domaine public USA' => 'PD-US',
	'Domaine public Ukraine' => 'PD',
	'Domaine public drapeau' => 'PD-Flag',
	'Domaine public expiré' => 'PD-old',
	'Domaine public expiré 50' => 'PD',
	'Domaine public expiré 70' => 'PD',
	'Domaine public gouvernement USA' => 'PD-USGov',
	'Domaine public par auteur' => 'PD-author',
	'Domaine public par utilisateur' => 'PD-user',
	'Domaine public perso' => 'PD-self',
	'Domaine public symbole Pologne' => 'PD-Polishsymbol',
	'Art Libre' => 'FAL',
	'ArtLibre' => 'FAL',
	'Image signée' => 'Watermark',
) ;
$transwiki_templates['he'] = array (
	'שימוש חופשי מוגן' => 'Copyrighted free use',
	'ייחוס' => 'Attribution',
	'צילום משתמש' => 'Attribution',
	'שימוש חופשי' => 'PD-self',
	'שימוש חופשי מוגן בתנאי' => 'Copyrighted free use provided that',
	'דו-רישיוני' => 'Self',
	'דו-רשיוני' => 'Self',
) ;
$transwiki_templates['hu'] = array (
	'közkincs|user=' => 'PD-user|1=',
	'közkincs-saját' => 'PD-self',
	'közkincs-ismeretlen' => 'PD-HU-unkown',
	'közkincs-régi' => 'PD-old',
	'közkincs-usa' => 'PD-USGov',
	'közkincs-usa-NASA' => 'PD-USGov-NASA',
	'közkincs-brit' => 'PD-UKGov',
	'kettős-GFDL-cc-by-sa-3.0' => 'self|GFDL|cc-by-sa-3.0',
	'GFDL-saját' => 'GFDL-self',
	'GPL' => 'GPLv3', // a Commons GPL nevű sablonja v2 vagy későbbi, a magyar v3 vagy későbbi
	                  // [[commons:Template:GPL]] states v2 or later, [[w:hu:Sablon:GPL]] states v3 or later version
	'szabad képernyőkép' => 'Free screenshot',
	'wikipédia-képernyőkép' => 'Wikipedia-screenshot',
) ;
$transwiki_templates['is'] = array (
	'CC-by-2.5' => 'CC-by-2.5' ,
	'GFDL' => 'GFDL' ,
	'PD-Eigin' => 'PD-self' ,
	'PD-ineligible' => 'PD-ineligible' ,
) ;
$transwiki_templates['it'] = array (
	'GFDL-con-disclaimer' => 'GFDL-it',
	'GFDL-Utente-con-disclaimer' => 'self|author=$$UPLOADER$$|GFDL-it',
	'PD-Utente' => 'PD-self' ,
	'PD-utente' => 'PD-self' ,
	'PD-Autore' => 'PD-author' ,
	'PD-autore' => 'PD-author' ,
) ;
$transwiki_templates['ko'] = array (
	'GFDL|갱신=갱신' => 'GFDL|migration=relicense',
	'GFDL|갱신=부적합' => 'GFDL|migration=not-eligible',
	'GFDL|갱신=불필요' => 'GFDL|migration=redundant',
	'Cc-0' => 'Cc-zero',
	'MIT 허가서' => 'MIT',
	'PD-서울시공개자료' => 'PD-author |1=[[:ko:위키백과:서울시 지식공유 프로젝트|Seoul Special City]]',
	'공공누리 제1유형' => 'KOGL',
	'위키미디어 재단 저작권' => 'Copyright by Wikimedia',
	'Self|GFDL|cc-by-sa-3.0' => 'Self|author=$$UPLOADER$$|GFDL|cc-by-sa-3.0'
	
);
$transwiki_templates['lb'] = array (
	'bild-GFDL' => 'GFDL',
	'bild-CC-by-sa' => 'Cc-by-sa',
) ;
$transwiki_templates['lv'] = array (
	'sedols' => 'self|author=Jānis Sedols|Cc-by-sa-3.0'
) ;
$transwiki_templates['nl'] = array (
#	'Ewmulti' => 'self2|GFDL|cc-by-sa-2.5',
#	'Eigenwerk' => 'GFDL-self',
	'PD-eigen' => 'PD-self',
	'CBS-wijkkaart' => 'Statistics Netherlands map',
) ;
$transwiki_templates['mk'] = array (
	'ЈД-предадено' => 'PD-self' ,
) ;
$transwiki_templates['ro'] = array (
	'DP-personal' => 'PD-self',
) ;
$transwiki_templates['sr'] = array (
	'ГЛСД-ја' => 'GFDL-self',
	'ГЛСД-са-одрицањем' => 'GFDL-with-disclaimers',
	'Cc-by-3.0-rs' => 'Cc-by-3.0-rs',
	'Cc-by' => 'Cc-by',
	'Cc-by-2.5' => 'Cc-by-2.5',
	'Cc-by-2.0' => 'Cc-by-2.0',
	'Cc-by-3.0' => 'Cc-by-3.0',
	'Cc-by-sa-2.5' => 'Cc-by-sa-2.5',
	'Cc-by-sa-2.0' => 'Cc-by-sa-2.0',
	'Приписивање' => 'Attribution',
	'Јв-ја' => 'PD-self',
	'ЈВ-ја' => 'PD-self',
	'Јв-ја' => 'PD-self',
	'јв-ја' => 'PD-self',
	'ГЛСД-ја' => 'GFDL-self',
	'ГЛСД-претпоставља се' => 'GFDL-self',
	'ГЛСД-без-одрицања' => 'GFDL-no-disclaimers',
	'GLSD-ja' => 'GFDL-self',
	'DP-usuari' => 'PD-user',
	'DP-Propi' => 'PD-self'
) ;
$transwiki_templates['tr'] = array (
	'KM-Kişisel' => 'PD-self' ,
) ;
$transwiki_templates['vi'] = array (
	'GFDL-1.2' => 'GFDL-1.2' ,
) ;

$category_names = array (
	'bn' => 'বিষয়শ্রেণী',
	'de' => 'kategorie',
	'el' => 'κατηγορία',
	'en' => 'category',
	'fa' => 'رده',
	'hu' => 'kategória',
	'is' => 'flokkur',
	'ko' => '분류',
	'nl' => 'categorie',
	'ro' => 'categorie',
) ;

$self_texts = array (
	'selbstgemacht',
	'selbstgescannt',
	'eigenesfoto',
	'eigenesphoto',
	'selbstfotografiert',
	'selbstphotografiert',
	'selbstgezeichnet',
	'selbstgemalt',
	'selbsterstellt',
	'eigenezeichnung',
	'eigen werk',
	'saját munka',
) ;

$permission_texts = array (
	'PD-release' => 'Released into the public domain (by the author)',
	'PD-self' => 'Released into the public domain (by the author)',
	'GFDL-self' => 'Licensed under the [[GFDL]] by the author',
	'GFDL 1.2' => 'Released under the [[GNU Free Documentation License]] version 1.2',
	'GFDL' => 'Released under the [[GNU Free Documentation License]]',
	'PD' => 'This image is in the [[public domain]]',
	'PD-Art' => 'This image is in the [[public domain]]',
	'PD-ineligible' => 'This image is in the [[public domain]] because it is ineligible for copyright',
	'SELF2|GFDL|CC-BY-SA-2.5' => 'Dual-licensed under the [[GFDL]] and [[cc-by-sa-2.5]]',
	'PD-user-w|' => 'Released into the public domain (by the author)',
	'PD-old' => 'This image is in the [[public domain]] due to its age',
) ;

$information_keys = array (
	# bn
	'বর্ণনা' => 'Beschreibung' ,
	'উৎস' => 'Quelle' ,
	'প্রণেতা' => 'Urheber' ,
	'তারিখ' => 'Datum' ,
	'অনুমতি' => 'Genehmigung' ,
	'অন্যান্য সংস্করণ' => 'Andere Versionen',
	
	# cs
	'popis' => 'Beschreibung',
	'zdroj' => 'Quelle',
	'datum' => 'Datum',
	'autor' => 'Urheber',
	'povolení' => 'Genehmigung',
	'jiné verze' => 'Andere Versionen',

	# en
	'description' => 'Beschreibung' ,
	'source' => 'Quelle' ,
	'author' => 'Urheber' ,
	'date' => 'Datum' ,
	'permission' => 'Genehmigung' ,
	'other_versions' => 'Andere Versionen',

	# el
	'Περιγραφή' => 'Beschreibung' ,
	'Πηγή' => 'Quelle' ,
	'Δημιουργός' => 'Urheber' ,
	'Ημερομηνία' => 'Datum' ,
	'Άδεια χρήσης' => 'Genehmigung' ,
	
	# eo
	'priskribo' => 'Beschreibung',
	'fonto' => 'Quelle',
	'auxtoro' => 'Urheber',
	'dato' => 'Datum',
	'permeso' => 'Genehmigung',
	'aliaj versioj' => 'Andere Versionen',

	# fa
	'توضیحات' => 'Beschreibung' ,
	'منبع' => 'Quelle' ,
	'پدیدآور' => 'Urheber' ,
	'تاریخ' => 'Datum' ,
	'اجازه‌نامه' => 'Genehmigung' ,
	'دیگر نسخه‌ها' => 'Andere Versionen',

	# fi
	'kuvaus' => 'Beschreibung' ,
	'Lähde' => 'Quelle' ,
	'Tekijänoikeuksien haltija' => 'Urheber' ,
	'päiväys' => 'Datum' ,

	# he
	'תיאור' => 'Beschreibung' ,
	'מקור' => 'Quelle' ,
	'תאריך יצירה' => 'Datum' ,
	'יוצר' => 'Urheber' ,
	'אישורים והיתרים' => 'Genehmigung' ,
	'גרסאות אחרות' => 'Andere Versionen' ,

	# hu
	'leírás' => 'Beschreibung' ,
	'forrás' => 'Quelle' ,
	'dátum' => 'Datum' ,
	'szerző' => 'Urheber' ,
	'engedély' => 'Genehmigung' ,
	'más változatok' => 'Andere Versionen' ,
	
	# is
	'myndlýsing' => 'Beschreibung' ,
	'uppruni' => 'Quelle' ,
	'höfundaréttshafi' => 'Urheber' ,
	'dagsetning' => 'Datum' ,
	'leyfisupplýsingar' => 'Genehmigung' ,
	'aðrar_útgáfur' => 'Andere Versionen',

	# it
	'descrizione' => 'Beschreibung',
	'fonte' => 'Quelle',
	'data' => 'Datum',
	'autore' => 'Urheber',
	'licenza' => 'Genehmigung',

	# ka
	'აღწერა' => 'Beschreibung',
	'წყარო' => 'Quelle',
	'თარიღი' => 'Datum',
	'ავტორი' => 'Urheber',
	'უფლება' => 'Genehmigung',
	
	# ko
	'설명' => 'Beschreibung' ,
	'출처' => 'Quelle' ,
	'만든이' => 'Urheber' ,
	'날짜' => 'Datum' ,
	'저작권' => 'Genehmigung' ,

	# nl
	'beschrijving' => 'Beschreibung',
	'bron' => 'Quelle',
	'datum' => 'Datum',
	'auteur' => 'Urheber',
	'toestemming' => 'Genehmigung',

	# ro
	'Descriere' => 'Beschreibung',
	'Sursa' => 'Quelle',
	'Data' => 'Datum',
	'Autor' => 'Urheber',
	'Permisiune' => 'Genehmigung',
	'alte_versiuni' => 'Andere Versionen',

	# ru
	'Описание' => 'Beschreibung',
	'Источник' => 'Quelle',
	'Время создания' => 'Datum',
	'Автор' => 'Urheber',
) ;

$info_templates = array ( '!' , '!!' , 'Documentation modèle' , 'Méta bandeau licence' , 'Pour Commons' , 'Purger le cache', ) ;

$info_template_names = array (
	'bn' => 'তথ্য',
	'de' => 'Information',
	'cs' => 'Popis souboru',
	'el' => 'Πληροφορίες εικόνας',
	'en' => 'Information',
	'eo' => 'Informo por dosiero',
	'fa' => 'اطلاعات',
	'gl' => 'Information',
	'he' => 'מידע',
	'hu' => 'Információ' ,
	'is' => 'Mynd',
	'it' => 'Informazioni file',
	'ka' => 'ინფორმაცია',
	'ko' => '파일 정보',
	'nl' => 'Information',
	'ro' => 'Informații',
	'ru' => 'Изображение',
) ;

$ignore_local_templates = array (
	'PD-Layout',
	'!',
	'!-',
	'Pour Commons',
	'Documentation modèle',
	'Méta bandeau licence',
	'Purger le cache',
	'Verplaats naar Wikimedia Commons',
	'Bausteindesign1',
	'Bausteindesign2',
	'Bausteindesign3',
	'Bausteindesign4',
	'Lizenzdesign1',
	'Lizenzdesign2',
	'CC-Layout',
	'NowCommons',
	'Verplaats naar commons',
	'NowCommonsBot',
	'Free media',
	'Image other',
	'Imbox',
	'PD-auteur',
	'Lang',
	'M',
	'Immagine commons',
	'Přesunout na Common',
	'Ambox',
	'GNU-Layout',
	'License migration',
	'License migration announcement',
	'License migration complete',
	'License migration redundant',
	'License migration is redundant',
	'Nach Commons verschieben (bestätigt)',
	'!!' ,
	'Date' ,
	'Description missing' ,
	'En' ,
	'FULLROOTPAGENAME' ,
	'Hidden' ,
	'JULIANDAY' ,
	'Max/2' ,
	'Max' ,
	'Mbox' ,
	'Ns has subpages' ,
	'Page' ,
	'Red' ,
	'Template other' ,
	'Tfd' ,
	'Tl' ,
	'Tlp' ,
	'Tlx' ,
	'Rename media' ,
	'Tmbox/core' ,
	'Tmbox',
	'Commonsba' ,
	'Commonsba másolandó',
	'Sl',
	'Sablonhivatkozás leírással',
	'Angolul',
	'Ccbysa-hulink',
	'Transferabil la Commons' ,
	'MoveToCommons',
	'Transferabil la commons',
	'Tlc',
	'Copy to Wikimedia Commons',
	'TLC'
) ;

$bad_headings = array (
	'licensing',
	'licentie',
	'lizenz',
	'lizensierung',
	'lizenzinformation',
	'licenc' ,
	'licențiere' ,
) ;

$license_dupes = array (
	'GFDL-user-' => 'GFDL',
	'PD-user-w' => 'PD',
) ;

$deprecated_licenses = array (
	'PD' ,
	'PD-Russia' ,
) ;

$prevent_transfer_categories = array ( // Prevent transfer if any category starts with one of these texts
	'Wikipedia:Dateiüberprüfung',
	'সকল অ-উন্মুক্ত মিডিয়া',
	'All non-free media',
	'All_non-free_media' ,
	'Bizonytalan felhasználhatóságú képek' ,
) ;

$prevent_transfer_templates = array (
	'NoCommons',
	'Nietverplaatsen',
	'অ-উন্মুক্ত ব্যবহারের যৌক্তিক ভিত্তি',
	'Non-free_use_rationale',
	'Non-free use rationale'
) ;

?>
